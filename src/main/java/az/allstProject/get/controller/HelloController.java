package az.allstProject.get.controller;

import az.allstProject.get.dto.SayHello;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {

    @GetMapping
    public String sayHelloUser(@RequestBody SayHello user) {
        return "Hello from" + user.getName();
    }

}
