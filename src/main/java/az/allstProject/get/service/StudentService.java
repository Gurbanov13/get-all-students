package az.allstProject.get.service;

import az.allstProject.get.dto.StudentDto;
import az.allstProject.get.model.Student;

import java.util.List;

public interface StudentService {

    StudentDto createStudent(StudentDto dto);

    StudentDto getStudentById(Long id);

    StudentDto updateStudent(StudentDto dto);

    void deleteStudentById(Long id);

    List<StudentDto> findAllStudent();




}
