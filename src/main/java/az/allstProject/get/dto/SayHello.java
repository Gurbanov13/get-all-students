package az.allstProject.get.dto;

import lombok.Data;

@Data
public class SayHello {
    private String name;
}
